#ifndef LISTA_H
#define LISTA_H

typedef struct Lista Lista;
typedef struct Nodo Nodo;

typedef struct Lista1 Lista1;
typedef struct Nodo1 Nodo1;

struct Nodo{
    float val;
    Nodo *siguiente;
};

struct Lista{
    Nodo *inicio;
};

struct Nodo1{
    int va;
    Nodo1 *siguiente1;
};

struct Lista1{
    Nodo1 *inicio1;
};

Lista *lista_nueva(void);
void lista_agrega(Lista *L, float val);
void lista_muestra(const Lista *L);
void lista_libera(Lista *L);

Lista1 *lista_nueva1(void);
void lista_agrega1(Lista1 *A, int va);
void lista_muestra1(const Lista1 *A);
void lista_libera1(Lista1 *A);

#endif //LISTA_H

